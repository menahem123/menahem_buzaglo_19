﻿namespace formhw19
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.datechoos = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.MonthCalendar();
            this.SuspendLayout();
            // 
            // datechoos
            // 
            this.datechoos.AutoSize = true;
            this.datechoos.Location = new System.Drawing.Point(24, 219);
            this.datechoos.Name = "datechoos";
            this.datechoos.Size = new System.Drawing.Size(0, 13);
            this.datechoos.TabIndex = 2;
            this.datechoos.Click += new System.EventHandler(this.label1_Click);
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(27, 18);
            this.date.MaxSelectionCount = 99999999;
            this.date.Name = "date";
            this.date.SelectionRange = new System.Windows.Forms.SelectionRange(new System.DateTime(2010, 5, 8, 0, 0, 0, 0), new System.DateTime(2025, 5, 14, 0, 0, 0, 0));
            this.date.TabIndex = 1;
            this.date.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 261);
            this.Controls.Add(this.date);
            this.Controls.Add(this.datechoos);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label datechoos;
        private System.Windows.Forms.MonthCalendar date;



    }
}